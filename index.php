<!doctype html>
<html class="h-100" lang="en">

  <head>
  <meta charset="utf-8">
  <link rel="icon" type="image/png" sizes="96x96" href="icon/icon.png">

  <title>Chart</title>
  <link rel="stylesheet" href="css/theme.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> 
  <style>
html,
body,
.intro {
  height: 100%;
}

table td,
table th {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}

.card {
  border-radius: .5rem;
}

.table-scroll {
  border-radius: .5rem;
}

thead {
  top: 0;
  position: sticky;
}
/* inter-300 - latin */
@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: local(''),
       url('./fonts/inter-v12-latin-300.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
       url('./fonts/inter-v12-latin-300.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
}

@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: local(''),
       url('./fonts/inter-v12-latin-500.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
       url('./fonts/inter-v12-latin-500.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
}
@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: local(''),
       url('./fonts/inter-v12-latin-700.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
       url('./fonts/inter-v12-latin-700.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
}

</style>

  </head>
  <body data-bs-spy="scroll" data-bs-target="#navScroll">
    <nav id="navScroll" class="navbar navbar-expand-lg navbar-light fixed-top" tabindex="0">
      <div class="container">
        <a class="navbar-brand pe-4 fs-4" href="index.php">
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-layers-half" viewbox="0 0 16 16">
            <path d="M8.235 1.559a.5.5 0 0 0-.47 0l-7.5 4a.5.5 0 0 0 0 .882L3.188 8 .264 9.559a.5.5 0 0 0 0 .882l7.5 4a.5.5 0 0 0 .47 0l7.5-4a.5.5 0 0 0 0-.882L12.813 8l2.922-1.559a.5.5 0 0 0 0-.882l-7.5-4zM8 9.433 1.562 6 8 2.567 14.438 6 8 9.433z"/>
          </svg>
          <span class="ms-1 fw-bolder">Chart by Faycal</span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item"></li>
          </ul>
          <a href="graph.php" data-splitbee-event="Click Download" aria-label="Download this template" class="link-dark pb-1 link-fancy me-2">Données importantes</a>
        </div>
      </div>
    </nav>
    <main>

      <br><br><br><br><br>
      <div class="small py-vh-3 w-100 overflow-hidden">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
              <div class="d-flex">
                <div class="col-md-3 flex-fill pt-3 pt-3 pe-3 pe-md-0">
                  <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" fill="currentColor" class="bi bi-window-sidebar" viewbox="0 0 16 16">
                    <path d="M2.5 4a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm2-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm1 .5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                    <path d="M2 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm12 1a1 1 0 0 1 1 1v2H1V3a1 1 0 0 1 1-1h12zM1 13V6h4v8H2a1 1 0 0 1-1-1zm5 1V6h9v7a1 1 0 0 1-1 1H6z"/>
                  </svg>
                </div>
                <div class="col-md-9 flex-fill">
                  <h3 class="h5 my-2">Concept</h3>
                  <p>
                    Chart by Faycal est une application de statistique de logement par région en France.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
              <div class="d-flex">
                <div class="col-md-3 flex-fill pt-3 pt-3 pe-3 pe-md-0">
                  <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" fill="currentColor" class="bi bi-window-sidebar" viewbox="0 0 16 16">
                    <path d="M2.5 4a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm2-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm1 .5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                    <path d="M2 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm12 1a1 1 0 0 1 1 1v2H1V3a1 1 0 0 1 1-1h12zM1 13V6h4v8H2a1 1 0 0 1-1-1zm5 1V6h9v7a1 1 0 0 1-1 1H6z"/>
                  </svg>
                </div>
                <div class="col-md-9 flex-fill">
                  <h3 class="h5 my-2">Recherche</h3>
                  <p>
                    Il vous est possible de rechercher des données via la barre de recherche ci-dessous qui va tenter de faire correspondre votre saisi à 3 critères
                    (Code du département, Code de la région ou l'année).
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
              <div class="d-flex">
                <div class="col-md-3 flex-fill pt-3 pt-3 pe-3 pe-md-0">
                  <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" fill="currentColor" class="bi bi-window-sidebar" viewbox="0 0 16 16">
                    <path d="M2.5 4a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm2-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm1 .5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                    <path d="M2 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm12 1a1 1 0 0 1 1 1v2H1V3a1 1 0 0 1 1-1h12zM1 13V6h4v8H2a1 1 0 0 1-1-1zm5 1V6h9v7a1 1 0 0 1-1 1H6z"/>
                  </svg>
                </div>
                <div class="col-md-9 flex-fill">
                  <h3 class="h5 my-2">Données importantes</h3>
                  <p>
                    Une section "Données importantes" est disponible afin de consulter les graphiques de données les plus intéressants et les plus recherchés
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container py-vh-3 border-top" data-aos="fade" data-aos-delay="200">
        <div class="row d-flex justify-content-center">
          <div class="col-12 col-lg-8 text-center">
            <h3 class="fs-2 fw-light">Mes données</h3>
          </div>

          <?php
          if(isset($_GET['search']) && !empty($_GET['search'])){
            $search = $_GET["search"];
          }else{
            $search = '';
          }
          ?>

          <div class="col-12 col-lg-8 text-center">
            <div class="row">
              <div class="grouped-inputs border bg-light p-2">
                <div class="row">
                  <div class="col">
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get" class="form-floating">
                      <input type="text" class="form-control p-3" id="search" placeholder="" name="search" value="<?php echo $search; ?>">
                      <label for="search">(Année, Code région ou Code département)</label>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php 
    if(isset($_GET['search'])){
    ?>
    <section class="intro">
    <div class="bg-image h-100">
      <div class="mask d-flex align-items-center h-100">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12">
              <div class="card shadow-2-strong">
                <div class="card-body p-0">
                  <div class="table-responsive table-scroll" data-mdb-perfect-scrollbar="true" style="position: relative; height: 700px">
                    <table class="table  mb-0">
                      
                      <tbody>
                        
                        <?php
                          $c=0;
                          $chemin_du_fichier_csv = 'data/logement.csv';
                          // Vérifier si le fichier existe
                          if (($handle = fopen($chemin_du_fichier_csv, 'r')) !== false) {
                              while (($data = fgetcsv($handle, 1000000, ';')) !== false) {
                                
                                if($c == 0){
                                  echo '<tr style=" background-color : darksalmon; top: 0; position: sticky;  ">';
                                }else{
                                  echo '<tr>';
                                }
                                if($data[0] == $_GET['search'] || $data[1] == $_GET['search'] || $data[3] == $_GET['search']){
                                  foreach($data as $d){
                                    echo '<td>';
                                      echo $d;
                                    echo '<td>';
                                  }
                                }elseif($c == 0 || empty($_GET['search'])){
                                  foreach($data as $d){
                                    echo '<td>';
                                      echo $d;
                                    echo '<td>';
                                  }
                                }
                                
                                $c++;
                                echo '</tr>';
                              }

                              // Fermer le fichier
                              fclose($handle);
                          } else {
                              // Gérer l'erreur si le fichier ne peut pas être ouvert
                              echo "Erreur: Impossible d'ouvrir le fichier CSV.";
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br><br> 
    </div>
  </section>
  <?php }else{
    echo '<div style="text-align:center">';
    echo "vous n'avez pas fais de recherche.. <br> <br> ";
    echo '<button type="button"" class="btn btn-primary"><a href="index.php?search="   style="text-decoration :none; color: white;">Afficher toutes les données </a></button>';
    echo '</div>';

  } ?>




  <br><br>
  <div style="text-align :center;"><a href="index.php"><img src="icon/logo.png" ></a></div>
  </main>


<script src="js/aos.js"></script>
 <script>
 AOS.init({
   duration: 800, // values from 0 to 3000, with step 50ms
 });
 </script>

 <script>
  let scrollpos = window.scrollY
  const header = document.querySelector(".navbar")
  const header_height = header.offsetHeight

  const add_class_on_scroll = () => header.classList.add("scrolled", "shadow-sm")
  const remove_class_on_scroll = () => header.classList.remove("scrolled", "shadow-sm")

  window.addEventListener('scroll', function() {
    scrollpos = window.scrollY;

    if (scrollpos >= header_height) { add_class_on_scroll() }
    else { remove_class_on_scroll() }

    console.log(scrollpos)
  })
  
</script>

  </body>
</html>
