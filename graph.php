<!doctype html>
<html class="h-100" lang="en">

  <head>
  <meta charset="utf-8">
  <link rel="icon" type="image/png" sizes="96x96" href="icon/icon.png">

  <title>Chart</title>
  <link rel="stylesheet" href="css/theme.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
html,
body,
.intro {
  height: 100%;
}

table td,
table th {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}

.card {
  border-radius: .5rem;
}

.table-scroll {
  border-radius: .5rem;
}

thead {
  top: 0;
  position: sticky;
}
/* inter-300 - latin */
@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: local(''),
       url('./fonts/inter-v12-latin-300.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
       url('./fonts/inter-v12-latin-300.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
}

@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: local(''),
       url('./fonts/inter-v12-latin-500.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
       url('./fonts/inter-v12-latin-500.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
}
@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: local(''),
       url('./fonts/inter-v12-latin-700.woff2') format('woff2'), /* Chrome 26+, Opera 23+, Firefox 39+ */
       url('./fonts/inter-v12-latin-700.woff') format('woff'); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
}

</style>

  </head>
  <body data-bs-spy="scroll" data-bs-target="#navScroll">
<nav id="navScroll" class="navbar navbar-expand-lg navbar-light fixed-top" tabindex="0">
      <div class="container">
        <a class="navbar-brand pe-4 fs-4" href="index.php">
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-layers-half" viewbox="0 0 16 16">
            <path d="M8.235 1.559a.5.5 0 0 0-.47 0l-7.5 4a.5.5 0 0 0 0 .882L3.188 8 .264 9.559a.5.5 0 0 0 0 .882l7.5 4a.5.5 0 0 0 .47 0l7.5-4a.5.5 0 0 0 0-.882L12.813 8l2.922-1.559a.5.5 0 0 0 0-.882l-7.5-4zM8 9.433 1.562 6 8 2.567 14.438 6 8 9.433z"/>
          </svg>
          <span class="ms-1 fw-bolder">Chart by Faycal</span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item"></li>
          </ul>
          <a href="graph.php" data-splitbee-event="Click Download" aria-label="Download this template" class="link-dark pb-1 link-fancy me-2">Données importantes</a>
        </div>
      </div>
    </nav>
    <main>
    <?php
        $chemin_du_fichier_csv = 'data/logement.csv';

        if(isset($_GET['region']) && !empty($_GET['region'])){
        $region = $_GET["region"];
        }else{
        $region = '';
        }
    ?>
      <br><br><br><br><br>
      <?php 
            $c =0;
            $regiondispo = array();
            if (($handle = fopen($chemin_du_fichier_csv, 'r')) !== false) {

                while (($data = fgetcsv($handle, 1000000, ';')) !== false) {
                    foreach($data as $d){         
                        if ($c == 4 && !(in_array($d, $regiondispo))) {
                            $regiondispo[] = $d;
                        }   
                        $c++;                    
                    }
                    $c = 0;
                }

                // Fermer le fichier
                fclose($handle);
            } else {
                // Gérer l'erreur si le fichier ne peut pas être ouvert
                echo "Erreur: Impossible d'ouvrir le fichier CSV.";
            }
                    
        ?>
        <div class="col-2 ">
        <div class="row">
            <div class="grouped-inputs border bg-light p-2" >
            <div class="row">
                <div class="col"  style="text-align:center;">
                <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get" class="form-floating"  value="<?php echo $region; ?>">
                    <select class="form-select" name="region" >
                    <option selected>Région</option>
  
                    <?php foreach($regiondispo as $rd){
                        echo '<option value="'.$rd.'">'.$rd.'</option>';

                    }
                    ?>
                    </select>
                    <br>
                    <input type="submit" value="Filtrer" class="btn btn-primary">
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>

      <div class="small py-vh-3 w-100 overflow-hidden">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
              <div class="d-flex">
                <div class="col-md-3 flex-fill pt-3 pt-3 pe-3 pe-md-0">
                <i class="" style="font-size:45px"></i>
                </div>
                <div class="col-md-9 flex-fill">
                  <h3 class="h5 my-2" style="white-space: nowrap;">Nb d'habitants /<?php echo $region; ?></h3>
                    <div style="">
                        <?php
                        $c = 0;

                        
                        $year=['2018','2019','2020','2021','2022'];
                        foreach($year as $y){
                            ${'stat'.$y} = 0;                            
                        }
                        $datapush = array();
                        // Vérifier si le fichier existe
                        if (($handle = fopen($chemin_du_fichier_csv, 'r')) !== false) {
                            while (($data = fgetcsv($handle, 1000000, ';')) !== false) {
                                foreach($data as $d){         
                                    if ($data[0] == $year[0] && $data[4] == $region && $c == 5) {
                                        $stat2018 = ${'stat'.$year[0]} + $d;
                                    }                       
                                    if ($data[0] == $year[1] && $data[4] == $region && $c == 5) {
                                        $stat2019 = ${'stat'.$year[1]} + $d;
                                    }
                                    if ($data[0] == $year[2] && $data[4] == $region && $c == 5) {
                                        $stat2020 = ${'stat'.$year[2]} + $d;
                                    }
                                    if ($data[0] == $year[3] && $data[4] == $region && $c == 5) {
                                        $stat2021 = ${'stat'.$year[3]} + $d;
                                    }
                                    if ($data[0] == $year[4] && $data[4] == $region && $c == 5) {
                                        $stat2022 = ${'stat'.$year[4]} + $d;
                                    }
                                    $c++;
                                }
                                $c = 0;
                            }
                            foreach($year as $y){
                                $datapush[] = ${'stat'.$y};                            
                            }


                            // Fermer le fichier
                            fclose($handle);
                        } else {
                            // Gérer l'erreur si le fichier ne peut pas être ouvert
                            echo "Erreur: Impossible d'ouvrir le fichier CSV.";
                        }
                        // Convertir le tableau PHP en JSON pour l'utiliser côté client
                        $json_data = json_encode($datapush);
                        ?>

                        <!-- Exemple d'utilisation de Chart.js côté client -->
                        <canvas id="myChart"></canvas>
                        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
                        <script>
                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = <?php echo $json_data; ?>;
                            
                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: {
                                    labels: ['2018', '2019', '2020', '2021', '2022'],
                                    datasets: [{
                                        label: 'Habitants /An',
                                        data: data,
                                    }]
                                }
                            });
                            
                          
                        </script>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
              <div class="d-flex">
                <div class="col-md-3 flex-fill pt-3 pt-3 pe-3 pe-md-0">
                <i class="" style="font-size:45px"></i>
                </div>
                <div class="col-md-9 flex-fill">
                  <h3 class="h5 my-2" style="white-space: nowrap;">Taux de chômage /<?php echo $region; ?></h3>
                    <div>
                    <div >
        <canvas id="myBarChart"></canvas>
    </div>
    <?php
        $c = 0;
        $div =0;
        
        $year=['2018','2019','2020','2021','2022'];
        foreach($year as $y){
            ${'stat'.$y} = 0;                            
        }
        $datapush = array();
        // Vérifier si le fichier existe
        if (($handle = fopen($chemin_du_fichier_csv, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000000, ';')) !== false) {
                foreach($data as $d){         
                    if ($data[0] == $year[0] && $data[4] == $region && $c == 12) {
                        $stat2018 = (${'stat'.$year[0]} + $d)/2;
                    }                       
                    if ($data[0] == $year[1] && $data[4] == $region && $c == 12) {
                        $stat2019 = (${'stat'.$year[1]} + $d)/2;
                    }
                    if ($data[0] == $year[2] && $data[4] == $region && $c == 12) {
                        $stat2020 = (${'stat'.$year[2]} + $d)/2;
                    }
                    if ($data[0] == $year[3] && $data[4] == $region && $c == 12) {
                        $stat2021 = (${'stat'.$year[3]} + $d)/2 ;
                    }
                    if ($data[0] == $year[4] && $data[4] == $region && $c == 12) {
                        $stat2022 = (${'stat'.$year[4]} + $d)/2;
                    }
                    $c++;
                }
                $c = 0;
            }
            foreach($year as $y){
                $datapush[] = ${'stat'.$y};                            
            }


            // Fermer le fichier
            fclose($handle);
        } else {
            // Gérer l'erreur si le fichier ne peut pas être ouvert
            echo "Erreur: Impossible d'ouvrir le fichier CSV.";
        }
        // Convertir le tableau PHP en JSON pour l'utiliser côté client
        $json_data = json_encode($datapush);
    ?>
    <script>
// Données fictives pour l'exemple
var data = {
    labels: ['2018','2019', '2020', '2021','2022'],
    datasets: [{
        label: 'Taux de Chômage /An',
        data: <?php echo $json_data; ?>, // Taux de chômage pour chaque année
        backgroundColor: [
            'rgba(255, 99, 132, 0.7)',
            'rgba(54, 162, 235, 0.7)',
            'rgba(75, 192, 192, 0.7)'
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(75, 192, 192, 1)'
        ],
        borderWidth: 1
    }]
};

// Options du graphique
var options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        y: {
            beginAtZero: true,
            title: {
                display: true,
                text: '(%)'
            }
        }
    }
};

// Récupérer le contexte du canvas
var ctx = document.getElementById('myBarChart').getContext('2d');

// Créer le graphique en barres
var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: options
});
    </script>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
              <div class="d-flex">
                <div class="col-md-3 flex-fill pt-3 pt-3 pe-3 pe-md-0">
                <i class="" style="font-size:45px"></i>
                </div>
                <div class="col-md-9 flex-fill">
                    <h3 class="h5 my-2" style="white-space: nowrap;">Nb de construction /<?php echo $region; ?></h3>
                    <div>
                    <div style="width: 80%; margin: auto;">
        <canvas id="myLineChart"></canvas>
    </div>
    <?php
        $c = 0;
        $div =0;
        
        $year=['2018','2019','2020','2021','2022'];
        foreach($year as $y){
            ${'stat'.$y} = 0;                            
        }
        $datapush = array();
        // Vérifier si le fichier existe
        if (($handle = fopen($chemin_du_fichier_csv, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000000, ';')) !== false) {
                foreach($data as $d){         
                    if ($data[0] == $year[0] && $data[4] == $region && $c == 20) {
                        $stat2018 = ${'stat'.$year[0]} + $d;
                    }                       
                    if ($data[0] == $year[1] && $data[4] == $region && $c == 20) {
                        $stat2019 = ${'stat'.$year[1]} + $d;
                    }
                    if ($data[0] == $year[2] && $data[4] == $region && $c == 20) {
                        $stat2020 = ${'stat'.$year[2]} + $d;
                    }
                    if ($data[0] == $year[3] && $data[4] == $region && $c == 20) {
                        $stat2021 = ${'stat'.$year[3]} + $d ;
                    }
                    if ($data[0] == $year[4] && $data[4] == $region && $c == 20) {
                        $stat2022 = ${'stat'.$year[4]} + $d;
                    }
                    $c++;
                }
                $c = 0;
            }
            foreach($year as $y){
                $datapush[] = ${'stat'.$y};                            
            }


            // Fermer le fichier
            fclose($handle);
        } else {
            // Gérer l'erreur si le fichier ne peut pas être ouvert
            echo "Erreur: Impossible d'ouvrir le fichier CSV.";
        }
        // Convertir le tableau PHP en JSON pour l'utiliser côté client
        $json_data = json_encode($datapush);
    ?>
    <script>
        // Données fictives pour l'exemple
        var data = {
            labels: ['2018','2019', '2020', '2021','2022'],
            datasets: [{
                label: 'Construction/An',
                data: <?php echo $json_data ?>, // Taux de chômage pour chaque année
                fill: false,
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 2,
                pointBackgroundColor: 'rgba(75, 192, 192, 1)',
                pointRadius: 5
            }]
        };

        // Options du graphique
        var options = {
            responsive: true,
            maintainAspectRatio: false,
            
        };

        // Récupérer le contexte du canvas
        var ctx = document.getElementById('myLineChart').getContext('2d');

        // Créer le graphique en ligne
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });
    </script>        
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



  <br><br>
  <div style="text-align :center; margin-top :8%;"><a href="index.php"><img src="icon/logo.png" ></a></div>
  </main>


<script src="js/aos.js"></script>
 <script>
 AOS.init({
   duration: 800, // values from 0 to 3000, with step 50ms
 });
 </script>

 <script>
  let scrollpos = window.scrollY
  const header = document.querySelector(".navbar")
  const header_height = header.offsetHeight

  const add_class_on_scroll = () => header.classList.add("scrolled", "shadow-sm")
  const remove_class_on_scroll = () => header.classList.remove("scrolled", "shadow-sm")

  window.addEventListener('scroll', function() {
    scrollpos = window.scrollY;

    if (scrollpos >= header_height) { add_class_on_scroll() }
    else { remove_class_on_scroll() }

    console.log(scrollpos)
  })
  
</script>
</body>
</html>